import { h, app } from 'hyperapp'
import { Icon } from '../../src/components/core/Icon'
import './ButtonsPreview.less'
import { icons } from 've-ui-theme'

export const IconsPreview = ({ }, children) => (
  <div>
    <Icon src={icons.edit} size='xs' />
    <Icon src={icons.edit} size='sm' />
    <Icon src={icons.edit} size='md' />
    <Icon src={icons.edit} size='lg' />
    <Icon src={icons.edit} size='xl' />
    <Icon src={icons.edit} size='xxl' />
    <br /><br />
    <Icon src={icons.edit}  />
    <Icon src={icons.edit} color="body" />
    <Icon src={icons.edit} color="danger" />
    <Icon src={icons.edit} color="primary" />
  </div>
)