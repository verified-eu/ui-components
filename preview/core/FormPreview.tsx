import { h, app } from 'hyperapp'
import { Form, Input, Label, InputError, FormGroup, RadioButton, Checkbox, TextArea, Switch } from '../../src/components/core'
import '../../src/components/core/Tooltip/Tooltip.less'
import './FormPreview.less'


const ButtonClick = () => {
  console.log('ButtonClick')
}

export const FormPreview = ({state}, children) => (
  <div style={{ padding: '30px' }}>
    <Form>
      <Label>Just a sad  label</Label>
      <div>
        <Input type='text' placeholder='name@example.com' />
      </div>
      <FormGroup>
        <Label>With label</Label>
        <Input type='password' />
      </FormGroup>
      <div>
        <Label>Disabled</Label><br/>
        <Input type='text' dataKey='Compact' value="Compact"/><br/>
        <Input type='text' disabled value="Disabled"/><br/>
        <Input type='text' disabled placeholder="Placeholder, disabled"/>
      </div>
      <div>
        <Label>Invalid</Label><br/>
        <Input type='text' invalid value="Invalid" />
      </div>
      <div style={{margin: "50px", padding: "30px", background: "#fee"}}>
        <Input type='text' placeholder='name@example.com' invalid />
        <span>icon</span>
        <InputError>Ooops, email is invalid</InputError>
      </div>
      <div>
        <TextArea dataKey='Compact' resizable rows="3" value="Compact"></TextArea>
        <TextArea resizable rows="3">aa</TextArea>
        <br/>
        <TextArea rows="3">aa</TextArea>
      </div>
    </Form>

    <span tooltip='hahahhaaaaa'>I have a tooltip</span>

    <Switch id='sisi1' onClick={ButtonClick} >SWITCH!!!!!! checked</Switch>
    <br/>
    <Switch id='sisi2' onClick={ButtonClick} align='right'>Right aligned</Switch>
    <br/>
    <Switch id='sisi3' onClick={ButtonClick} disabled>Disabled</Switch>
    <br/><br/>
    <RadioButton id='haha2' checked={false} align='right' name="group 1" >aaaaa checked</RadioButton>
    <br/><br/>
    <RadioButton id='haha3' checked={false} disabled name="group 1" >Disabled</RadioButton>
    <br/>
    <RadioButton id='haha4' checked disabled name="group 1" >Disabled, active</RadioButton>
    <br/><br/>
    <RadioButton id='haha5' checked  name="group 1">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse vestibulum, nisi vel dictum blandit, orci libero consectetur erat, id placerat lorem purus eget sem. </RadioButton>

    <br/><br/><br/><br/>

    <Checkbox id='hihi1' checked={false} align='right' >aaaaa checked</Checkbox>
    <br/><br/>
    <Checkbox id='hihi2' checked={false} disabled >Disabled</Checkbox>
    <br/>
    <Checkbox id='hihi3' checked disabled >Disabled, active</Checkbox>
    <br/><br/>
    <Checkbox id='hihi4' checked >Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse vestibulum, nisi vel dictum blandit, orci libero consectetur erat, id placerat lorem purus eget sem. </Checkbox>


  </div>
)
