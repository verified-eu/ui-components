import { h, app } from 'hyperapp'
import { ButtonsPreview, AlertsPreview, IconsPreview, DropdownsPreview, FormPreview, ModalsPreview } from './index.ts'
import { tooltips } from '../..';
import './styles.less';

app({
  init: {
    dropdownCollapsed: false,
    dropdown2Collapsed: false,
    disabled1: false,
  },
  view: state => (
    <div data-key='App'>
      <br/><br/><br/><br/>
      <h1>Heading 1</h1>
      <h2>Heading 2</h2>
      <h3>Heading 3</h3>
      <h4>Heading 4</h4>
      <h5>Heading 5</h5>
      <h6>Heading 6</h6>
      <span style={{float: 'right', color: '#f00'}}
        tooltip="2"
        tooltip-position='bottom-left'
        tooltip-size='xl'
      >hahahaha NO</span>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse vestibulum, nisi vel dictum blandit, orci libero consectetur erat, id placerat lorem purus eget sem. Fusce ultrices, felis id vestibulum suscipit, mauris diam feugiat leo, at mollis lorem ligula nec quam. Nam condimentum dictum nulla, id consequat sem mattis vel. </p>
      <h3>
        <u><b><i tooltip='Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse vestibulum, nisi vel dictum blandit, orci libero consectetur erat, id placerat lorem purus eget sem. Fusce ultrices, felis id vestibulum suscipit, mauris diam feugiat leo, at mollis lorem ligula nec quam. Nam condimentum dictum nulla, id consequat sem mattis vel.'
      tooltip-position='right'
       tooltip-size='xl'>la di daa</i></b></u>
      </h3>
      {/* --------------------------------------- Core components */}
      <ButtonsPreview /> 
      <AlertsPreview />
      <IconsPreview />
      <DropdownsPreview state={state} />
      <FormPreview state={state} />
      <ModalsPreview state={state} />
    </div>
  ),
  container: document.body
})
