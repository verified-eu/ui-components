import { h, app } from 'hyperapp'
import { Alert } from '../..'
import { icons } from 've-ui-theme'

const state = {}
const actions = {}

const ButtonClick = () => {
  console.log('ButtonClick')
}

export const AlertsPreview = ({}, children) => (
  <div>
    <Alert fixed role='warning'  dismissable onDismiss={ButtonClick} heading="Heading">warning dismissable!!!!!!!!!!</Alert>
    {/* <Alert fixed role='warning' dismissable onDismiss={ButtonClick} closeIcon={icons.cancelCircleLines}>warning dismissable!!!!!!!!!!</Alert> */}
    <br/><br/><br/><br/>
    <Alert role='success'>This is a simple alert</Alert>
    <br/>
    <Alert heading='Heading' dismissable role='danger'>No role. Alert with a header</Alert>
    <br/>
    <Alert role='success' dismissable heading='Success'>success</Alert>
    <br/>
    <Alert dismissable heading='Default'>default</Alert>
    <br/>
    <Alert role='warning' dismissable heading='Warning'>
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse vestibulum, nisi vel dictum blandit, orci libero consectetur erat, id placerat lorem purus eget sem. Fusce ultrices, felis id vestibulum suscipit, mauris diam feugiat leo, at mollis lorem ligula nec quam. Nam condimentum dictum nulla, id consequat sem mattis vel
    </Alert>
  </div>
)