import { h, app } from 'hyperapp'
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem, DropdownDivider } from '../../src/components/core'
import './DropdownsPreview.less'

const ToggleDropdown = state => ({ ...state, dropdownCollapsed: !state.dropdownCollapsed })
const ToggleDropdown2 = state => ({ ...state, dropdown2Collapsed: !state.dropdown2Collapsed })
const ToggleDropdown3 = state => (
  console.log('alooo')
)

export const DropdownsPreview = ({state}, children) => (
  <div>
    <Dropdown fullWidth  pristine dataKey='Dropdown1'>
      <DropdownToggle truncate={false} onClick={ToggleDropdown}>A very very very very very very long name</DropdownToggle>
      <DropdownMenu fullWidth scrollable collapsed={state.dropdownCollapsed} >
        <DropdownItem onClick={ToggleDropdown3}>My settings</DropdownItem>
        <DropdownItem onClick={ToggleDropdown3}><img src="https://via.placeholder.com/24x16/F60/333"/>A very very very very very very long name</DropdownItem>
        <DropdownItem active>Company settings</DropdownItem>
        <DropdownItem>Disabled</DropdownItem>
        <DropdownItem disabled>Disabled</DropdownItem>
        <DropdownItem><span>Span</span> <strong>this</strong></DropdownItem>
        <DropdownItem><img src="https://via.placeholder.com/24x16/F60/333"/>Disabled</DropdownItem>
        <DropdownItem>My settings</DropdownItem>
        <DropdownItem>My settings</DropdownItem>
        <DropdownDivider />
        <DropdownItem disabled>Logout</DropdownItem> 
      </DropdownMenu>
    </Dropdown>

    <Dropdown fullWidth  pristine dataKey='Dropdown2'>
      <DropdownToggle truncate={false} onClick={ToggleDropdown2}><img src="https://via.placeholder.com/24x16/F60/333"/>A very very very very very</DropdownToggle>
      <DropdownMenu fullWidth scrollable collapsed={state.dropdownCollapsed} >
        <DropdownItem>My settings</DropdownItem>
        <DropdownItem><img src="https://via.placeholder.com/24x16/F60/333"/>A very very very very very very long name</DropdownItem>
        <DropdownItem active>Company settings</DropdownItem>
        <DropdownItem>Disabled</DropdownItem>
        <DropdownItem disabled>Disabled</DropdownItem>
        <DropdownItem><span>Span</span> <strong>this</strong></DropdownItem>
        <DropdownItem><img src="https://via.placeholder.com/24x16/F60/333"/>Disabled</DropdownItem>
        <DropdownItem>My settings</DropdownItem>
        <DropdownItem>My settings</DropdownItem>
        <DropdownDivider />
        <DropdownItem disabled>Logout</DropdownItem>
      </DropdownMenu>
    </Dropdown>
  </div>
)