import { h, app } from 'hyperapp'
import { Button, ButtonGroup } from '../../src/components/core'
import './ButtonsPreview.less'

export const ButtonsPreview = ({}, children) => (
  <div>
    <h2>bbbbbbbbb</h2>
    <h3>hahaha <Button variant='inline'>Inline</Button></h3>
    <span>lkdnfkjhfsd kf sdkfks dk fsdkf <Button variant='inline'>Inline</Button></span>
    <div style={{background: '#FFF'}}> 
      <Button>Default</Button>
      <Button variant='inline'>Inline</Button>
      <Button variant='link'>Link</Button>
      <Button disabled dataKey="Primary">Disabled</Button>
      <Button dataKey="MinWidth">Min</Button>
      
      <Button dataKey="Primary">Primary</Button>
      <Button dataKey="Danger">Danger</Button>
      <Button dataKey="Outline--Primary">Outline primary</Button>
      <Button dataKey="Outline--Danger">Outline danger</Button>
      <Button dataKey="Rounded">Rounded primary Full</Button>
      <Button dataKey="Rounded"><img src="https://via.placeholder.com/24x16/F60/333"/>With Icon</Button>
    </div>
  </div>
)