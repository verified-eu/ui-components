import { h, app } from 'hyperapp'
import { Modal, ModalHeader, ModalBody, ModalFooter, Button } from '../../src/components/core'
import './ModalsPreview.less'
import { data } from 'autoprefixer';

let isShown = false

const hide = (state) => {
  isShown = false
  return { ...state }
}

const show = (state) => {
  isShown = true
  return { ...state }
}

export const ModalsPreview = ({state}, children) => (
  <div style={{ padding: '30px' }}>
      <div>
        <Button onClick={show} role='primary'>Show Pop Up</Button>

        <Modal onClose={hide} show={isShown} dataKey='Magic'>
          <ModalHeader onClose={hide}>Discard envelope?</ModalHeader>
          <ModalBody>
              Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
      <br /><br />
              Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
      <br /><br />
              Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
      <br /><br />
              Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
      <br /><br />
              Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
      <br /><br />
              Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
      <br /><br />
              Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
            <br /><br />
          </ModalBody>
          <ModalFooter>
            <Button role='primary'>Discard</Button>
            <Button role='primary'>Save as draft</Button>
            <Button onClick={hide} role='primary'>Back</Button>
          </ModalFooter>
        </Modal>
      </div>

  </div>
)
