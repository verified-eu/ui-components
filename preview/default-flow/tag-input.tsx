import { app, h } from 'hyperapp'
import { TagInput, NoEffect } from '../../index'
import  '../core/styles.less'

app({
  init: {
    envelope: {
      docs: {
        '123': {
          name: 'aaa.pdf'
        },
        '124': {
          name: 'aaa.pdf',
          tags: ['aa', 'bb']

        }
      },
      predefinedTags: ['set', 'in', 'stone-long-ass-tag-name-to-break-long-stuff']
    }
  },
  view: (state) => {
    return (
      <div style={{background: "#FFF"}}>
        <div style={{ width: '550px', border: '1px solid #eee' }}>
          <TagInput key="i1" state={state} path={['envelope', 'docs', '123']} expandedPath={['_active']} />
          <hr />
          <TagInput key="i2" state={state} path={['envelope', 'docs', '124']} expandedPath={['_active']} predefinedTags={state.envelope.predefinedTags} />
        </div>
        <hr />
        <pre>
          {JSON.stringify(state, 2, 2)}
        </pre>
      </div>
    )
  },
  container: document.body,
  subscriptions: (state) => {
    let clickOutside = null
    return [
      state._active && [
        (action, dispatch) => {
          clickOutside = event => dispatch(action, event)
          window.addEventListener('click', clickOutside)
        }, [
          (state, next, event) => next(state, event),
          (state, event) => {
            return [{ ...state, _active: null }, NoEffect()]
          },
          () => window.removeEventListener('click', clickOutside) 
        ]
      ]
    ]
  }
})
