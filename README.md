[//]: # (block-start:title)

# UI components (DEPRECATED)

[//]: # (block-stop:title)

[//]: # (block-start:summary)

An ancient implementation of UI components using the Verified theme and the Hyperapp framework, which seemed to be the go-to solution on the front-end side. 

[//]: # (block-stop:summary)