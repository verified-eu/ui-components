import { h } from 'hyperapp'
import * as style from './Breadcrumbs.less'
import { Icon } from '../../core'
import { BreadcrumbsProps } from './BreadcrumbsTypes'

export const Breadcrumbs = ({ items, step, maxStep, dataKey, className, onItemClick }: BreadcrumbsProps) => (
  <div
    data-key={dataKey}
    data-ui='Breadcrumbs'
    className={style['breadcrumbs'] + ' ' + (className || '')}
  >
    {items.map((item, index) =>
      <button
        key={index.toString()}
        className={
          style['breadcrumb-item'] +
          (step === index ? ' ' + style['active'] : '') +
          (index < step ? ' ' + style['visited'] : '')
        }
        onClick={(maxStep > index ? [onItemClick, index] : null)}
        disabled={index >= maxStep}
      >
        <Icon
          src={item.icon}
          size='sm'
          color={(index >= maxStep ? 'secondary' : 'primary')}
          className={style['breadcrumb-icon']}
        />
        {step === index && <span className={style['breadcrumb-label']}>{item.label}</span>}
      </button>
    )}
  </div>
)
