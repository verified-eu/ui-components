/* eslint-disable */
import { app, h } from 'hyperapp'
/* eslint-enable */
import * as style from './TagInput.less'
import { Icon } from '../../core/Icon'
const SVGCancel = require('../../../assets/cancel.svg')

export interface TagProps {
  key: string
  toggled?: boolean
  onClick? (...args: any[])
}

export const Tag = ({ key, toggled, onClick, disabled }: TagProps, children) => (
  <div data-ui='TagInput__Tag' key={key} className={style['tag'] + (toggled ? ' ' + style['toggled'] : '') + (disabled ? ' ' + style['disabled'] : '')}>
    <span className={style['name']}>{children}</span>
    <span className={style['remove']} onClick={onClick}>
      <Icon src={SVGCancel} color='default' role='cancel' size='xs' />
    </span>
  </div>
)
