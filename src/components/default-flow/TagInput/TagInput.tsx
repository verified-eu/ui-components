/* eslint-disable */
import { app, h } from 'hyperapp'
/* eslint-enable */
import { defaultTo, path, lensPath, set } from 'ramda'
import { uid } from '../../utils/IUID'
import { NoEffect } from '../../../../index'
import * as style from './TagInput.less'
import { Tag } from './Tag'
import { DropdownItem, DropdownMenu } from '../../core/Dropdown'
import { TextHighlight } from './TextHighlight'

const focus = (state, inputId) => {
  setTimeout(() => {
    window.document.getElementById(inputId).focus()
  }, 10)
  return [state, NoEffect()]
}

const onTagInput = (tagInput: TagInputModel) => (state, props, event) => {
  const value = event.target.value
  if (value !== tagInput.searchValue) {
    return tagInput.onSearchTags(state, value)
  }
  return [{...state}, NoEffect()]
}

const keyUp = (tagInput: TagInputModel) => (state, props, event) => {
  const value = event.target.value
  switch (event.key) {
    case 'Backspace':
      if (value === '' && tagInput.tags.length) {
        if (!tagInput.deleteArmed) {
          return tagInput.armDelete(state)
        } else {
          return tagInput.onRemoveTag(state, tagInput.tags.length - 1)
        }
      }
      break
    case 'ArrowDown':
      return tagInput.changeSelectedTag(state, ++tagInput.selectedIndex)
    case 'ArrowUp':
      return tagInput.changeSelectedTag(state, --tagInput.selectedIndex)
    case 'Enter': case ' ':
      return tagInput.onAddTag(state, value)
    default:
      return [state, NoEffect()]
  }
}

const onSearchTags = (tagInput: TagInputModel) => (state, searchedVal:string, event:MouseEvent) => {
  event && event.stopPropagation()
    searchedVal = event ? event.target.value : searchedVal
  let newState = { ...state }
  newState = set(lensPath(tagInput.path.concat(['_tagInput', 'key'])), tagInput.key, newState)
  newState = set(lensPath(tagInput.path.concat(['_tagInput', 'deleteArmed'])), false, newState)
  newState = set(lensPath(tagInput.expandedPath), tagInput.key, newState)
  newState = set(lensPath(tagInput.path.concat(['_tagInput', 'searchValue'])), searchedVal, newState)
  newState = set(lensPath(tagInput.path.concat(['_tagInput', 'selectedIndex'])), 0, newState)
  newState = set(lensPath(tagInput.path.concat(['_tagInput', 'suggestionsLoading'])), true, newState)

  return tagInput.searchTags(newState, searchedVal, event)
}

const searchTags = (tagInput: TagInputModel) => (state, searchedVal:string, event:MouseEvent) => {
  let mockJS = ['tag 1', 'tag 2', 'tag 3']
  if (!searchedVal) {
    mockJS.push('tag 4')
    mockJS.push('tag 5')
  } else {
    mockJS.push(searchedVal)
    mockJS.push(`${searchedVal} but close`)
  }
  let newState = set(lensPath(tagInput.path.concat(['_tagInput', 'suggestions'])), mockJS, state)
  newState = set(lensPath(tagInput.path.concat(['_tagInput', 'suggestionsLoading'])), false, newState)
  return [newState, NoEffect()]
}
const onAddTag = (tagInput: TagInputModel) => (state, tag:string, event:MouseEvent) => {
  event && event.stopPropagation()
  let newState = set(lensPath(tagInput.path.concat(['_tagInput', 'deleteArmed'])), false, state)
  newState = set(lensPath(tagInput.path.concat(['_tagInput', 'selectedIndex'])), 0, newState)
  newState = set(lensPath(tagInput.expandedPath), null, newState)
  newState = set(lensPath(tagInput.path.concat(['_tagInput', 'searchValue'])), '', newState)
  if (tagInput.selectedIndex < tagInput.suggestions.length && !event) {
    tag = tagInput.suggestions[tagInput.selectedIndex]
  }
  if (tag && tagInput.tags.findIndex(t => t === tag) === -1 && tagInput.predefinedTags.findIndex(t => t === tag) === -1) {
    return tagInput.addTag(newState, tag, event)
  } else {
    return [newState, NoEffect()]
  }
}
const addTag = (tagInput: TagInputModel) => (state, tag:string, event:MouseEvent) => {
  return [set(lensPath(tagInput.path.concat(['tags'])), tagInput.tags.concat([tag]), state), NoEffect()]
}

const onRemoveTag = (tagInput: TagInputModel) => (state, index) => {
  let newState = { ...state }
  newState = set(lensPath(tagInput.path.concat(['_tagInput', 'deleteArmed'])), false, newState)
  newState = set(lensPath(tagInput.path.concat(['_tagInput', 'key'])), tagInput.key, newState)
  return tagInput.removeTag(newState, index, [(state) => {
    return focus(state, `${tagInput.key}-input`)
  }])
}

const removeTag = (tagInput: TagInputModel) => (state, index, fx = NoEffect()) => {
  tagInput.tags.splice(index, 1)
  return [set(lensPath(tagInput.path.concat(['tags'])), tagInput.tags, state), fx]
}
const changeSelectedTag = (tagInput: TagInputModel) => (state, index) => {
  let newState = set(lensPath(tagInput.path.concat(['_tagInput', 'deleteArmed'])), false, state)
  if (index < tagInput.suggestions.length && index >= 0) {
    newState = set(lensPath(tagInput.path.concat(['_tagInput', 'selectedIndex'])), index, newState)
  }
  return [newState, NoEffect()]
}
const armDelete = (tagInput: TagInputModel) => (state) => {
  return [set(lensPath(tagInput.path.concat(['_tagInput', 'deleteArmed'])), true, state), NoEffect()]
}
class TagInputModel {
  state?: any
  path?: Array<string | number>
  expandedPath?: Array<string | number>
  key: string
  selectedIndex?: number
  searchValue?: string
  placeholder?: string
  maxLength?: number
  predefinedTags?: string[]
  tags?: string[]
  deleteArmed?: boolean
  suggestions?: string[]
  suggestionsLoading?: boolean
  onSearchTags?(...args: any[])
  searchTags?(...args: any[])
  onAddTag?(...args: any[])
  addTag?(...args: any[])
  onRemoveTag?(state: any, index: number)
  removeTag?(state: any, index: number)
  changeSelectedTag?(...args: any[])
  armDelete?(...args: any[])
  keyUp?(...args: any[])
  onTagInput?(...args: any[])
  constructor (props:TagInputModel) {
    this.state = defaultTo({})(path(['state'], props))
    this.path = defaultTo([''])(path(['path'], props))
    this.expandedPath = defaultTo(['_expanded'])(path(['expandedPath'], props))
    this.key = defaultTo(uid.generate())(path(this.path.concat(['_tagInput', 'key']), this.state))
    this.suggestions = defaultTo([])(path(this.path.concat(['_tagInput', 'suggestions']), this.state))
    this.searchValue = defaultTo('')(path(this.path.concat(['_tagInput', 'searchValue']), this.state))
    this.selectedIndex = defaultTo(0)(path(this.path.concat(['_tagInput', 'selectedIndex']), this.state))
    this.tags = defaultTo([])(path(this.path.concat(['tags']), this.state))
    this.deleteArmed = defaultTo(false)(path(this.path.concat(['_tagInput', 'deleteArmed']), this.state))
    this.suggestionsLoading = defaultTo(false)(path(this.path.concat(['_tagInput', 'suggestionsLoading']), this.state))
    this.placeholder = defaultTo('Search here')(path(['placeholder'], props))
    this.maxLength = defaultTo(20)(path(['maxLength'], props))
    this.predefinedTags = defaultTo([])(path(['predefinedTags'], props))
    this.onSearchTags = defaultTo(onSearchTags(this))(path(['onSearchTags'], props))
    this.searchTags = defaultTo(searchTags(this))(path(['searchTags'], props))
    this.onAddTag = defaultTo(onAddTag(this))(path(['onAddTag'], props))
    this.addTag = defaultTo(addTag(this))(path(['addTag'], props))
    this.onRemoveTag = defaultTo(onRemoveTag(this))(path(['onRemoveTag'], props))
    this.removeTag = defaultTo(removeTag(this))(path(['removeTag'], props))
    this.changeSelectedTag = defaultTo(changeSelectedTag(this))(path(['changeSelectedTag'], props))
    this.armDelete = defaultTo(armDelete(this))(path(['armDelete'], props))
    this.keyUp = defaultTo(keyUp(this))(path(['keyUp'], props))
    this.onTagInput = defaultTo(onTagInput(this))(path(['onTagInput'], props))
  }
}

export const TagInput = (props:TagInputModel, children) => {
  const tagInput = new TagInputModel(props)
  const showTagsDropdown = !tagInput.suggestionsLoading && tagInput.suggestions && tagInput.suggestions.length > 0
  return (<div>
    <div className={style['taginput-wrapper']}>
      {tagInput.predefinedTags.map(tag => (
        <Tag disabled>{tag}</Tag>
      ))}
      {tagInput.tags.filter(tag => !tagInput.predefinedTags.includes(tag)).map((tag, index) => (
        <Tag onClick={[tagInput.onRemoveTag, tag]}
          toggled={tagInput.deleteArmed && index === tagInput.tags.length - 1}>{tag}</Tag>
      ))}

      <div className={style['input-wrapper']}>
        <input id={`${tagInput.key}-input`} type='text' autocomplete='off'
          className={style['input']}
          value={tagInput.searchValue}
          placeholder={tagInput.placeholder}
          aria-label={tagInput.placeholder}
          maxLength={tagInput.maxLength}
          onKeyUp={[tagInput.keyUp]}
          onInput={[tagInput.onTagInput]}
          onClick={[tagInput.onSearchTags]}
        />
        { showTagsDropdown &&
          <DropdownMenu collapsed={tagInput.key !== path(tagInput.expandedPath, tagInput.state)}>
            {tagInput.suggestionsLoading && (<DropdownItem active={false}>
              <div className={style['tag-spinner']} />
            </DropdownItem>)}
            {!tagInput.suggestionsLoading && tagInput.suggestions.map((tag, index) => (
              <DropdownItem active={index === tagInput.selectedIndex} onClick={[tagInput.onAddTag, tag]}>
                <TextHighlight text={tag} highlight={tagInput.searchValue} />
              </DropdownItem>
            ))}
          </DropdownMenu>
        }
      </div>
    </div>
  </div>)
}
