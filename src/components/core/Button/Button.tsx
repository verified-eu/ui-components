import { h } from 'hyperapp'
import { PropsButton, PropsButtonGroup } from './ButtonTypes'
import * as styles from './Button.less'

export const Button = ({ active, role, onClick, key, dataKey, dropdownToggle, className, id, block, type, disabled, href, target, download, variant }: PropsButton, children) => (
  <button
    data-key={dataKey}
    data-ui='Button'
    className={
      (variant && variant!=='default'  ? styles['btn-' + variant] : styles.btn) +
      (block ? ' ' + styles['btn-block'] : '') +
      (active ? ' ' + styles.active + ' active' : '') +
      (role ? ' ' + styles['btn-' + role] : '') +
      (dropdownToggle ? ' ' + styles['dropdown-toggle'] : '') +
      (className ? ' ' + className : '')
    }
    key={key}
    disabled={disabled}
    download={download}
    id={id}
    href={href}
    onClick={onClick}
    target={target}
    type={type}
  >
    {children}
  </button>
)

export const ButtonGroup = ({ block, className, dataKey }: PropsButtonGroup, children) => (
  <div
    data-key={dataKey}
    data-ui='ButtonGroup'
    className={
      styles['btn-group'] +
      (block ? ' ' + styles['w-100'] : '') +
      (className ? ' ' + className : '')
    }
  >
    {children}
  </div>
)
