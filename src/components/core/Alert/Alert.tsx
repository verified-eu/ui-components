import { h } from 'hyperapp'
import { Icon } from '../Icon/Icon'
import { icons } from 've-ui-theme'
import * as styles from './Alert.less'
import { PropsAlert } from './AlertTypes'

export const Alert = ({ role, dataKey, className, closeIcon, dismissable, heading, fixed, onDismiss }: PropsAlert, children) => {
  const dismiss = dismissable && (
    <button
      data-ui='Alert--Close'
      className={styles['alert-close']}
      type='button'
      onClick={onDismiss}
    >
      {fixed && <Icon src={icons.cancelCircleLines} size='md'/>}
      {!fixed && <Icon src={icons.cancel} size='sm'/>}
    </button>
  )

  return (
    <div
      data-key={dataKey}
      data-ui={fixed ? 'Alert--Fixed' : 'Alert'}
      className={
        (fixed ? styles['alert-fixed'] : styles.alert) +
        (role ? ' ' + styles['alert-' + role] : ' ' + styles['alert-default'] ) + 
        (dismissable ? ' ' + styles['alert-dismissible'] : '') +
        (className ? ' ' + className : '')
      }
    >
      {heading && (
        <header className={styles['alert-heading'] + (role ? ' ' + styles['alert-heading-' + role] : '')}>
          {heading}
        </header>
      )}
      <footer className={styles['alert-body']}>
        {children}
      </footer>
      {dismiss}
    </div>
  )
}
