export interface PropsModal {
  dataKey?: string
  onClose? (...args: any[])
  show?: boolean
}

export interface PropsModalHeader {
  dataKey?: string
  onClose? (...args: any[])
}
export interface PropsModalBody {
  dataKey?: string
}
export interface PropsModalFooter {
  dataKey?: string
}
