import { h } from 'hyperapp'
import { Icon } from '../Icon/Icon'
import { icons } from 've-ui-theme'
import { PropsModal, PropsModalHeader, PropsModalBody, PropsModalFooter } from './ModalTypes'
import * as styles from './Modal.less'

const stopPropagation = (state, event) => {
  event.stopPropagation()
  return state
}

export const Modal = ({ show = false, onClose = null, dataKey }: PropsModal, children) => {
  return (
    <div
      data-key={dataKey}
      data-ui='Modal'
      onClick={onClose}
      className={
        styles['modal'] + ' ' + styles['fade'] +
        (show ? ' ' + styles['show'] + ' ' + styles['d-block'] : '')
      }
      tabindex='-1'
      role='dialog'
    >
      <div
        data-ui='ModalDialog'
        onClick={stopPropagation}
        className={'modal-dialog ' + styles['modal-dialog'] + ' ' + styles['modal-dialog-centered']}
      >
        <div data-ui='ModalContent' className={styles['modal-content']}>
          {children}
        </div>
      </div>
    </div>
  )
}

export const ModalHeader = ({ onClose = null, dataKey }: PropsModalHeader, children) => {
  return (
    <div
      data-key={dataKey}
      data-ui='ModalHeader'
      className={styles['modal-header']}
    >
      <h3>{children}</h3>
      {onClose && <button onClick={onClose} type='button' className={styles['modal-close']}>
        <Icon src={icons.cancel} size='md' />
      </button>} 
    </div>
  )
}

export const ModalBody = ({ dataKey }:PropsModalBody, children) => {
  return (
    <div
      data-key={dataKey}
      data-ui='ModalBody'
      className={styles['modal-body']}
    >
      {children}
    </div>
  )
}

export const ModalFooter = ({ dataKey }:PropsModalFooter, children) => {
  return (
    <div
      data-key={dataKey}
      data-ui='ModalFooter'
      className={styles['modal-footer']}
    >
      {children}
    </div>
  )
}
