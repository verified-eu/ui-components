export interface PropsIcon {
  className?: string
  dataKey?: string
  src?: string
  size?: 'xs' | 'sm' | 'md' | 'lg' | 'xl' | 'xxl'
  color?: 'default' | 'muted' | 'primary' | 'white' | 'body' | 'background' | 'secondary' | 'danger'
  role?: string
}
