import { h } from 'hyperapp'
import {
  PropsForm,
  PropsLabel,
  PropsInput,
  PropsInputError,
  PropsCheckbox,
  PropsRadioButton,
  PropsTextArea,
  PropsFormGroup
} from './FormTypes'
import * as styles from './Form.less'

export const Form = ({ className, container, dataKey }: PropsForm, children) => (
  <form
    data-key={dataKey}
    data-ui='Form'
    className={(className ? ' ' + className : '')}
  >
    {children}
  </form>
)

export const Label = ({ className, htmlFor, dataKey, onClick }: PropsLabel, children) => (
  <label
    data-key={dataKey}
    data-ui='Label'
    className={styles.label + (className ? ' ' + className : '')}
    onClick={onClick}
    for={htmlFor}
  >
    {children}
  </label>
)

export const FormGroup = ({ className, dataKey }: PropsFormGroup, children) => (
  <label
    data-key={dataKey}
    data-ui='FormGroup'
    className={styles['form-group'] + (className ? ' ' + className : '')}
  >
    {children}
  </label>
)

export const Input = ({ className, autocomplete, disabled, invalid, dataKey, name, value, placeholder, required, type, onInput, onClick, onFocus, onBlur, onPaste }: PropsInput) => (
  <input
    data-key={dataKey}
    data-ui='Input'
    className={
      styles['form-control'] +
      (invalid ? ' ' + styles['is-invalid'] : '') +
      (className ? ' ' + className : '')
    }
    type={type}
    autocomplete={autocomplete}
    placeholder={placeholder}
    disabled={disabled}
    required={required}
    onInput={onInput}
    onClick={onClick}
    onFocus={onFocus}
    onBlur={onBlur}
    onPaste={onPaste}
    name={name}
    value={value}
  />
)

export const TextArea = ({ className, disabled, invalid, dataKey, name, placeholder, required, rows, resizable, value, onInput, onClick, onFocus, onBlur, onPaste }: PropsTextArea) => (
  <textarea
    data-key={dataKey}
    data-ui='TextArea'
    className={
      styles['form-control'] +
      (invalid ? ' ' + styles['is-invalid']  : '') +
      (resizable ? '' : ' ' + styles['non-resizable']) +
      (className ? ' ' + className : '')
    }
    placeholder={placeholder}
    disabled={disabled}
    required={required}
    onInput={onInput}
    onClick={onClick}
    onFocus={onFocus}
    onBlur={onBlur}
    onPaste={onPaste}
    name={name}
    rows={rows}
    value={value}
  />
)

export const InputError = ({ className, dataKey }: PropsInputError, children) => (
  <div
    data-key={dataKey}
    data-ui='InputError'
    className={styles['invalid-feedback'] + (className ? ' ' + className : '')}
  >
    {children}
  </div>
)

export const Checkbox = ({ id, className, disabled, required, checked, align, value, onClick, onChange, key, dataKey }: PropsCheckbox, children) => (
  <div
    data-key={dataKey}
    data-ui='Checkbox'
    className={ 
                                 styles['custom-control'] +
                           ' ' + styles['custom-checkbox'] +
      (align === 'right' ? ' ' + styles['custom-control-right'] : ' ' + styles['custom-control-left']) +
      (className ? ' ' + className : '')
    }
    key={key}
  >
    <input
      type='checkbox'
      className={styles['custom-control-input']}
      id={id}
      disabled={disabled}
      required={required}
      checked={checked}
      value={value}
      onChange={onChange}
    />
    <label
      className={styles['custom-control-label']}
      for={id}
      onClick={onClick}
    >
      {children}
    </label>
  </div>
)

export const RadioButton = ({ id, name, className, disabled, required, checked, align, value, onClick, onChange, key, dataKey }: PropsRadioButton, children) => (
  <div
    data-key={dataKey}
    data-ui='RadioButton'
    className={
                                 styles['custom-control'] +
                           ' ' + styles['custom-radio'] +
      (align === 'right' ? ' ' + styles['custom-control-right'] : ' ' + styles['custom-control-left']) +
      (className ? ' ' + className : '')
    }
    key={key}
  >
    <input
      type='radio'
      className={styles['custom-control-input']}
      id={id}
      name={name}
      disabled={disabled}
      required={required}
      checked={checked}
      value={value}
      onChange={onChange}
    />
    <label
      className={styles['custom-control-label']}
      for={id}
      onClick={onClick}
    >
      {children}
    </label>
  </div>
)
