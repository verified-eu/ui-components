export interface PropsLink {
  onClick?
  href?: string
  active?: boolean
  className?: string
  target?: '_self' | '_blank' | '_parent' | '_top'
  download?: boolean
  key?: string
  dataKey?: string
  disabled?: boolean
}

export interface PropsClass {
  className?: string
  dataKey?: string
}

export interface PropsDropdownMenu {
  align?: 'left' | 'right'
  collapsed?: boolean
  fullWidth?: boolean
  scrollable?: boolean
  className?: string
  dataKey?: string
}

export interface PropsDropdownToggle {
  onClick?
  className?: string
  dataKey?: string
  disabled?: boolean
  truncate?: boolean
}

export interface PropsDropdown {
  fullWidth?: boolean
  invalid?: boolean
  className?: string
  dataKey?: string
  pristine?: boolean
}
