import { h } from 'hyperapp'
import { PropsDropdown, PropsDropdownMenu, PropsDropdownToggle, PropsLink, PropsClass } from './DropdownTypes'
import * as styles from './Dropdown.less'

export const Dropdown = ({ className, dataKey, fullWidth, pristine, invalid }: PropsDropdown, children) => (
  <div
    data-key={dataKey}
    data-ui={'Dropdown' + (pristine ? ' Dropdown--Pristine' : '')}
    className={
      styles['dropdown'] +
      (fullWidth ? ' ' + styles['w-full'] : '') +
      (invalid ? ' ' + styles['dropdown-invalid'] : '') +
      (pristine ? ' ' + styles['dropdown-pristine'] : '') +
      (className ? ' ' + className : '')
    }
  >
    {children}
  </div>
)

export const DropdownToggle = ({ className, dataKey, onClick, truncate = true, disabled }: PropsDropdownToggle, children) => (
  <button
    data-key={dataKey}
    data-ui='DropdownToggle'
    className={
      styles['dropdown-toggle'] +
      (truncate ? ' ' + styles.truncate : ' ' + styles['no-truncate']) +
      (className ? ' ' + className : '')
    }
    disabled={disabled}
    onClick={onClick}
    type='button'
    data-toggle='dropdown'
  >
    {children.length > 0 ? children : <b>&nbsp;</b> }
  </button>
)

export const DropdownMenu = ({ collapsed, dataKey, align, className, scrollable, fullWidth }: PropsDropdownMenu, children) => (
  <div
    data-key={dataKey}
    data-ui='DropdownMenu'
    className={
      styles['dropdown-menu'] +
      (collapsed ? '' : ' ' + styles['show']) +
      (fullWidth ? ' ' + styles['w-full'] : '') +
      (scrollable ? ' ' + styles['dropdown-max-items'] + ' ' + styles['dropdown-scrollable'] : '') +
      (align === 'right' ? ' ' + styles['dropdown-menu-right'] : '') +
      (className ? ' ' + className : '')
    }
  >
    {children}
  </div>
)

export const DropdownItem = ({ onClick, active, key, dataKey, href, className, target, download, disabled }: PropsLink, children) => (
  <a
    data-key={dataKey}
    data-ui='DropdownItem'
    className={
      styles['dropdown-item'] +
      (active ? ' ' + styles['active'] : '') +
      (disabled ? ' ' + styles['disabled'] : '') +
      (className ? ' ' + className : '')
    }
    onClick={onClick}
    href={href}
    target={target}
    download={download}
    key={key}
    disabled={disabled}
  >
    {children}
  </a>
)

export const DropdownDivider = ({ className, dataKey }: PropsClass) => (
  <div
    data-key={dataKey}
    data-ui='DropdownDivider'
    className={styles['dropdown-divider'] + (className ? ' ' + className : '')}
  />
)
