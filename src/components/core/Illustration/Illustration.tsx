import * as styles from './Illustration.less'
import { h } from 'hyperapp'
import { PropsIllustration } from './IllustrationTypes'

export const Illustration = ({ className, dataKey, src }: PropsIllustration) => (
  <span
    data-key={dataKey}
    data-ui='Illustration'
    className={styles['illustration'] + ' ' + (className || '')}
    innerHTML={src.file}
    style={{ width: src.width, height: src.height }}
  />
)
