import * as flags from './flags'
import * as spinners from './spinners'

export const tooltips = require('./src/components/core/Tooltip/Tooltip.less')

export * from './src/components/core'

export * from './src/components/default-flow/Breadcrumbs'
export * from './src/components/default-flow/Loader'
export * from './src/components/default-flow/TagInput'

export { flags, spinners }

export const NoEffect = () => ([() => { }])
export { uid } from './src/components/utils/IUID'
